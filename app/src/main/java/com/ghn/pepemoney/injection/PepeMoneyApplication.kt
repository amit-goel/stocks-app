package com.ghn.pepemoney.injection

import android.support.multidex.MultiDexApplication
import com.ghn.bottleRocket.timber.NotLoggingTree
import com.ghn.pepemoney.BuildConfig
import com.ghn.pepemoney.injection.module.AppModule
import com.ghn.pepemoney.injection.module.NetworkModule
import timber.log.Timber

class PepeMoneyApplication : MultiDexApplication() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
        else
            Timber.plant(NotLoggingTree())

        initializeDagger()
    }

    private fun initializeDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .roomModule(RoomModule())
                .networkModule(NetworkModule())
                .build()
    }
}