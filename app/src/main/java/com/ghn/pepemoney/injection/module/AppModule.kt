package com.ghn.pepemoney.injection.module

import android.content.Context
import com.ghn.pepemoney.injection.PepeMoneyApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Module which provides all required dependencies about Context
 */
@Module
class AppModule (private val pepeApplication: PepeMoneyApplication) {

    @Provides
    @Singleton
    fun provideContext(): Context = pepeApplication

}