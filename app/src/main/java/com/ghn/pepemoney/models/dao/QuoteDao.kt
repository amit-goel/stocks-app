package com.ghn.pepemoney.models.dao

import android.arch.persistence.room.*
import com.ghn.pepemoney.models.database.Quote
import io.reactivex.Flowable


@Dao
interface QuoteDao {

    @Query("SELECT * from quote")
    fun getAll(): Flowable<List<Quote>>

    @Query("SELECT * from quote where instrumentId in (:instrumentId)")
    fun get(instrumentId: List<Long?>): Flowable<List<Quote>>

    /**
     * Inserts specified posts in database.
     * @param quotes all the posts to insert in database
     */
    @Insert
    fun insertAll(quotes: List<Quote>) : List<Long>

    @Insert
    fun insert(quotes: Quote) :Long

    @Query("DELETE from quote")
    fun deleteAll()

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun updateAll(quotes: List<Quote>) : Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(quotes: Quote) : Int

}